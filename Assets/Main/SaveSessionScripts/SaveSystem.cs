﻿using System.IO;
using UnityEngine;

public static class SaveSystem
{

    private static string DirectoryPath;
    private static bool Initialized;
    


   public static void Init()
    {
        DirectoryPath = $"{Application.dataPath}/Saves/";

        if (!Directory.Exists(DirectoryPath))
        {
            Directory.CreateDirectory(DirectoryPath);
        }
        Initialized = true;        
    }

    public static void Save(object DataToSave, string FileName = "Fav", string FileExtension = "json")
    {
        if (!Initialized)
        {
            Init();
        }

        var currentTime = System.DateTime.Now.ToString().Split('μ')[0];
        //currentTime = currentTime.Replace(" ", " ");
        currentTime = currentTime.Replace("/", " ").ToString();
        currentTime = currentTime.Replace(":", "-").ToString();
        //currentTime = currentTime.Replace("π", "a").ToString();
        //currentTime = currentTime.Replace("μ", "p").ToString();
        currentTime = currentTime.Replace(".", "m").ToString();
        Debug.Log(currentTime);

        string JSONSave = JsonUtility.ToJson(DataToSave);

        if (File.Exists($"{DirectoryPath}{FileName}.json"))
        {
            File.Delete($"{DirectoryPath}{FileName}.json");
        }
        StreamWriter SaveFileWriter = new StreamWriter($"{DirectoryPath}{FileName + "_" + currentTime}.{FileExtension}");

        SaveFileWriter.WriteLine(JSONSave);
        SaveFileWriter.Close();
    }

    public static void Load<T>(out T LoadedData, string FileName = "Objects", string FileExtension = "json")
    {
        if (!Initialized)
        {
            Init();
        }

        StreamReader SaveFileReader = new StreamReader($"{DirectoryPath}{FileName}.{FileExtension}");
        string JSONSave = SaveFileReader.ReadLine();
        
        LoadedData = JsonUtility.FromJson<T>(JSONSave);
        SaveFileReader.Close();

    }






}
