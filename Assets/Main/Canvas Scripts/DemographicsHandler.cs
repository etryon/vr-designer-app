﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
[ExecuteInEditMode]
public class DemographicsHandler : MonoBehaviour
{
    
    
    private void Awake()
    {
        var dropdown = transform.GetComponent<Dropdown>();

        dropdown.options.Clear();

        List<string> demographics = new List<string>();

        //Age and Gender options
        demographics.Add("0-18 & female");
        demographics.Add("18-25 & female");
        demographics.Add("25-30 & female");
        demographics.Add("30-35 & female");
        demographics.Add("35-45 & female");
        demographics.Add("45-55 & female");
        demographics.Add(">55 & female");

        demographics.Add("0-18 & male");        
        demographics.Add("18-25 & male");        
        demographics.Add("25-30 & male");        
        demographics.Add("30-35 & male");        
        demographics.Add("35-45 & male");        
        demographics.Add("45-55 & male");        
        demographics.Add(">55 & male");
        
        //Fill dropdown with options
        foreach(var item in demographics)
        {
            dropdown.options.Add(new Dropdown.OptionData() { text = item });
        }
    }


    private void Update()
    {
        var Blocker = GameObject.Find("Blocker");
        if (Blocker)
        {
            Blocker.transform.localPosition = new Vector3(0, 0, 1310);
            Destroy(Blocker);
            return;
        }
    }
}
