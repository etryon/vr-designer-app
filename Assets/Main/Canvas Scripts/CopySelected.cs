﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.Networking;
using System.Globalization;

using Unity.Services.Core;
using Unity.Services.Analytics;


public class CopySelected : MonoBehaviour
{
    public GameObject selectedGarment;
    GameObject setPercentage;
    string DemographicChoice;
    double percentage;
    string percentageToString;
    string percentageToString2;
    string base64Image;
    string[] Split1;
    string[] Split2;

    String FinalText;
   
    Dropdown dd;
    //Trend class to create Trend Detection object to send
    [Serializable]
    public class TrendClass
    {
        //public string img_url; // Use this for a url pointing to an image.
        public string img_data; // Use this for a Base64 encoded string from a PNG image.
        public string demographic_group;
    }
    // Trend class for returned element from Mallzee API
    [Serializable]
    public class PredictionClass
    {
        public string demographic;
        public List<string> popularity;
        public string primary_score;
        public List<string> items;
    }
    [Serializable]
    public class TrendReturnClass
    {
        public List<PredictionClass> predictions;
    }

    // mallzee API Key
    // !! DONT UPLOAD TO VERSION CONTROL !!
    private readonly string mallzeeApiKey = "GL289xxXnsaukgV3Gxs7u5zDnTHechIwaZcQIdFk";
    GameObject copySelected;
    Vector3 position;
    private void Start()
    {
        
        position = new Vector3(9.3f, -49.4f, 0f);
        setPercentage = GameObject.Find("Percentage");
        dd = GameObject.Find("Dropdown").GetComponent<Dropdown>();
        DemographicChoice = dd.options[0].text;       
        dd.onValueChanged.AddListener(delegate {
            DropdownValueChanged(dd);
        });
    }
            
    void DropdownValueChanged(Dropdown dd)
    {        
        DemographicChoice = dd.options[dd.value].text;

        Dictionary<string, object> changeDemographicChoice = new Dictionary<string, object>()
        {
            { "demographicChange", DemographicChoice }
        };

        // The ‘myEvent’ event will get queued up and sent every minute
        AnalyticsService.Instance.CustomData("DemographicChange", changeDemographicChoice);
        AnalyticsService.Instance.Flush();

        setPercentage.GetComponent<Text>().text = "Please re-select a garment";   
    }
        

    void PercentageUpdating()
    {
        setPercentage.GetComponent<Text>().text = "Updating...";
    }    

    public void SetNewSelected()
    {
        
        if (selectedGarment.transform.childCount>0)
        {
            foreach (Transform child in selectedGarment.transform)
            {
                Destroy(child.gameObject);
            }
        }      

        copySelected = Instantiate(gameObject,new Vector3(0,0,0),Quaternion.identity);
        copySelected.transform.parent = selectedGarment.transform;   

        var rectTransform = copySelected.GetComponent<RectTransform>();
        //rectTransform.sizeDelta = new Vector2(0.3f, 0.3f);
        rectTransform.sizeDelta = new Vector2(1f, 1f);
        rectTransform.localScale = new Vector3(28f, 28f, 28f);
        
        //copySelected.transform.localPosition = new Vector3(-40.7f,0.6f,0f);
        copySelected.transform.localPosition = new Vector3(0f, 0f, 0f);
        copySelected.transform.localEulerAngles = new Vector3(0f, 0f, 0f);
        //Debug.Log(copySelected.transform.localPosition);
        //Disable Button Component;
        var component = copySelected.GetComponent<Button>();        
        component.enabled = false;

        // --------------------------------------------------------------------
        // Trend Detection

        // Get image texture
        var garmentTexture = copySelected.GetComponent<Image>().sprite.texture;

        string spriteName = garmentTexture.name;
        // Create byte array by encoding to png (Byte array is serializeable)
        var baseImage = Resources.Load(spriteName) as Texture2D;

        byte[] byteArr;
        //byteArr = garmentTexture.EncodeToPNG();
        byteArr = baseImage.EncodeToPNG();
        Debug.Log("Loaded from resources");

        // Now convert the byteArray to a Base64 string
        base64Image = Convert.ToBase64String(byteArr);

        // Send request to Trend Detection service
        DetectTrends(base64Image, DemographicChoice);
        
    }

    public void DetectTrends(string image, string demographic)
    {
        PercentageUpdating();
        StartCoroutine(SendTrendDetectionRequest(image, demographic));       
    }

    IEnumerator SendTrendDetectionRequest(string image, string demographic)
    {
        string url = "https://etryon.weareunfolded.com/products/predictions";

        // Create a new serialiazable object
        TrendClass trendObject = new TrendClass();
        trendObject.img_data = image;
        trendObject.demographic_group = demographic;

        // Serialize string into json
        string jsonString = JsonUtility.ToJson(trendObject);
        //Debug.Log(jsonString);

        // Create POST request
        var request = new UnityWebRequest(url, "POST");
        byte[] bodyRaw = Encoding.UTF8.GetBytes(jsonString); // Set right encoding
        request.uploadHandler = (UploadHandler)new UploadHandlerRaw(bodyRaw);
        request.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
        // Set request headers for authentication
        request.SetRequestHeader("Content-Type", "application/json");
        request.SetRequestHeader("x-api-key", mallzeeApiKey);
        yield return request.SendWebRequest();
        
        //Debug.Log("Status Code: " + request.responseCode);
        var data = request.downloadHandler.text;
        //Debug.Log("Data: " + data);

        if (request.responseCode == (long)System.Net.HttpStatusCode.OK) {

            //Take first data result and send it to UI
            TrendReturnClass returnObj = JsonUtility.FromJson<TrendReturnClass>(data);
            //float score = float.Parse(returnObj.predictions[0].primary_score, CultureInfo.InvariantCulture);

            if (returnObj.predictions[0].popularity.Count == 0)
            {
                FinalText = "No matching garment found";
            }
            else
            {
                float score1 = float.Parse(returnObj.predictions[0].popularity[0], CultureInfo.InvariantCulture);

                //percentage = Math.Round((score * 100), 2);

                double percentage1 = Math.Round((score1 * 100), 2);
                //Debug.Log(score1);
                //Debug.Log(percentage1);
                var item1 = returnObj.predictions[0].items[0].ToString();
                Split1 = item1.Split('|');
                
                if (score1 < 0.33f)
                {                    
                    percentageToString = "Low Popularity";
                }
                if (percentage1 >= 33f && percentage1 <= 66f)
                {                    
                    percentageToString = "Semi-popular";
                }
                if (percentage1 > 66f)
                {                    
                    percentageToString = "Highly Popular";
                }
                Debug.Log(percentageToString);
                FinalText = Split1[0] + " : " + percentageToString;
                //FinalText = Split1[0] + "  :  " + percentage1+"%";


                if (returnObj.predictions[0].popularity.Count > 1)
                {
                    float score2 = float.Parse(returnObj.predictions[0].popularity[1], CultureInfo.InvariantCulture);
                    double percentage2 = Math.Round((score2 * 100), 2);
                    var item2 = returnObj.predictions[0].items[1].ToString();
                    if (percentage2 < 33f)
                    {
                        percentageToString2 = "Low Popularity";
                    }
                    if (percentage2 >= 33f && percentage2 <= 66f)
                    {
                        percentageToString2 = "Semi-popular";
                    }
                    if (percentage2 > 66f)
                    {
                        percentageToString2 = "Highly Popular";
                    }

                    Split2 = item2.Split('|');
                    FinalText = Split1[0] + " : " + percentageToString + "\n" + Split2[0] + " : " + percentageToString2;
                    //FinalText = Split1[0] + " : " + percentage1 + "%" + "\n" + Split2[0] + " : " + percentage2 + "%";
                }
            }
            var data_test = request.downloadHandler.text;           
            Debug.Log(data_test);
            

            //percentageToString = percentage.ToString()+"%";            

            setPercentage.GetComponent<Text>().text = FinalText;
            //setPercentage.GetComponent<Text>().text = percentage.ToString();
        }
   
    }
}
