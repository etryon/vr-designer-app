﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using Unity.Services.Core;
using Unity.Services.Analytics;


public class NewFavorite : MonoBehaviour
{
    public GameObject currentGarment;
    GameObject activeGender;
    GameObject activeGarment;   
    
    public GameObject FavoritesTab;

    GameObject copyToFavs;

    public void FirstActive()
    {
        for (int i = 0; i < currentGarment.transform.childCount; i++)
        {
            if (currentGarment.transform.GetChild(i).gameObject.activeSelf == true)
            {               
                activeGender = currentGarment.transform.GetChild(i).gameObject;
            }
        }
        //Debug.Log(activeGender.name);

        for(int i = 0; i < activeGender.transform.GetChild(0).transform.childCount; i++)
        {
            if (activeGender.transform.GetChild(0).transform.GetChild(i).gameObject.activeSelf==true)
            {

                activeGarment = activeGender.transform.GetChild(0).transform.GetChild(i).gameObject;
                
            }
            //Debug.Log(activeGarment.name);
        }
    }
    public void AddToFavorites()
    {

        copyToFavs = Instantiate(activeGarment, new Vector3(0, 0, 0), Quaternion.identity);
        copyToFavs.transform.parent = FavoritesTab.transform;
        
            

        var rectTransform = copyToFavs.GetComponent<RectTransform>();
        //rectTransform.sizeDelta = new Vector2(0.3f, 0.3f);
        rectTransform.sizeDelta = new Vector2(1f, 1f);
        rectTransform.localScale = new Vector3(1f, 1f, 1f);

        //copySelected.transform.localPosition = new Vector3(-40.7f,0.6f,0f);
        copyToFavs.transform.localPosition = new Vector3(0f, 0f, 0f);
        copyToFavs.transform.localEulerAngles = new Vector3(0f, 0f, 0f);
        
        //Disable Button Component;
        var component = copyToFavs.GetComponent<Button>();
        component.enabled = true;


        Debug.Log("Garment: "+ activeGarment.name + " added to Favorites!");
        activeGarment.GetComponent<ObjectID>().enabled = false;


        Dictionary<string, object> custom4 = new Dictionary<string, object>()
        {
            { "addToFavs", activeGarment.name.ToString() }
        };

        AnalyticsService.Instance.CustomData("AddToFavs", custom4);
        AnalyticsService.Instance.Flush();


    }

    public void RemoveFromFavorites()
    {
        GameObject LastFav = FavoritesTab.transform.GetChild(FavoritesTab.transform.childCount - 1).gameObject;
        Destroy(LastFav);
    }
    public void ClearAllFacorites()
    {
        for(int i=0; i < FavoritesTab.transform.childCount; i++)
        {
            GameObject favs = FavoritesTab.transform.GetChild(i).gameObject;
            Destroy(favs);
        }
    }
}
