﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DynamicSpawn : MonoBehaviour
{

    Canvas canvasMode;

    private void Awake()
    {
        canvasMode = gameObject.GetComponent<Canvas>();        
    }

    private void OnEnable()
    {      
        canvasMode.renderMode = RenderMode.ScreenSpaceCamera;
        SwitchToWorld();
    }

    private void OnDisable()
    {
        canvasMode.renderMode = RenderMode.WorldSpace;
    }

    void SwitchToWorld()
    {
        canvasMode.renderMode = RenderMode.WorldSpace;
        //gameObject.transform.eulerAngles = new Vector3(0, 0, 0);
    }
}

